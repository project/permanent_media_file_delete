# Permanent Media File Delete module

Provides additional functionality for Media file delete to permanently remove
orphaned files if existing media is replaced with new one.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

This module requires media file delete module.

## Recommended modules


## Installation

Install as you would normally install a contributed Drupal module.

For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

## Maintainers

- [CHAITANYA_R_DESSAI] - [chaitanyadessai](https://www.drupal.org/u/chaitanyadessai)
