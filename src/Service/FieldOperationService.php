<?php

namespace Drupal\permanent_media_file_delete\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\file\FileUsage\FileUsageInterface;

/**
 * Class FieldOperationService declaration.
 */
class FieldOperationService {

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  private $fileStorage;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  private $fileUsage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager service.
   * @param \Drupal\file\FileUsage\FileUsageInterface $fileUsage
   *   The file usage service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, FileUsageInterface $fileUsage, Connection $database, MessengerInterface $messenger) {
    $this->fileStorage = $entityTypeManager->getStorage('file');
    $this->fileUsage = $fileUsage;
    $this->database = $database;
    $this->messenger = $messenger;
  }

  /**
   * Function performFieldOperation.
   */
  public function performFieldOperation(EntityInterface $entity, $field_name) {
    $current_file_id = $entity->get($field_name)->target_id;
    $previous_file_id = $entity->original->{$field_name}->target_id;
    if ($current_file_id != $previous_file_id) {
      // Count usages of the old file.
      $usage_count = $this->countFileUsage($previous_file_id);
      if ($usage_count <= 1) {
        // If the media has been replaced and the old file is not used
        // elsewhere, delete it.
        $old_file = $this->fileStorage->load($previous_file_id);
        if ($old_file) {
          $old_file->delete();
        }
        else {
          $this->messenger->addWarning('Cannot delete Media.');
        }
      }
    }
  }

  /**
   * Count usages of the file from the database.
   *
   * @param int $file_id
   *   The file ID.
   *
   * @return int
   *   The number of usages of the file.
   */
  private function countFileUsage($file_id) {
    return (int) $this->database->query("SELECT COUNT(*) FROM {file_usage} WHERE fid = :fid", [':fid' => $file_id])->fetchField();
  }

}
